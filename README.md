[![pipeline status](https://gitlab.com/grunlab/decimation/badges/main/pipeline.svg)](https://gitlab.com/grunlab/decimation/-/commits/main)

# GrunLab Decimation

Decimation non-root container image and deployment on Kubernetes.

Docs:
- https://docs.grunlab.net/images/decimation.md
- https://docs.grunlab.net/apps/decimation.md

Base image: [grunlab/base-image/ubuntu:22.04][base-image]

Format: docker

Supported architecture(s):
- amd64

[base-image]: <https://gitlab.com/grunlab/base-image>

