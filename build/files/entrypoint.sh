#!/bin/bash

JAVA_MAX_MEMORY=${JAVA_MAX_MEMORY:-500M}

FILES="banned-ips.json banned-players.json decimation_server.properties decimation_traders.json decimation_turfs.json decimation_zones.json ops.json server.properties usercache.json userData.json userInvites.json usernamecache.json whitelist.json"
DIRECTORIES="clans"

for FILE in $FILES; do
  if [ ! -f /mnt/decimation-config/$FILE ]; then touch /mnt/decimation-config/$FILE; fi
  ln -s /mnt/decimation-config/$FILE /opt/decimation/$FILE
done

for DIRECTORY in $DIRECTORIES; do
  if [ ! -d /mnt/decimation-config/$DIRECTORY ]; then mkdir /mnt/decimation-config/$DIRECTORY; fi
  ln -s /mnt/decimation-config/$DIRECTORY /opt/decimation/$DIRECTORY
done

ln -s /mnt/decimation-world /opt/decimation/world

screen -d -m -L -Logfile decimation.log -S decimation java -Xms${JAVA_MAX_MEMORY} -Xmx${JAVA_MAX_MEMORY} -server -XX:+UnlockExperimentalVMOptions -XX:MaxGCPauseMillis=50 -XX:+DisableExplicitGC -XX:TargetSurvivorRatio=90 -XX:G1NewSizePercent=50 -XX:G1MaxNewSizePercent=80 -XX:InitiatingHeapOccupancyPercent=10 -XX:G1MixedGCLiveThresholdPercent=50 -XX:+AggressiveOpts -jar thermos.jar nogui

while [ ! -f /opt/decimation/decimation.log ]; do sleep 1; done
tail -f decimation.log

